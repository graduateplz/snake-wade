{
    "id": "7e7cf7d7-3e94-4901-a9aa-89b4aaac180b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_hardClick",
    "eventList": [
        {
            "id": "97c9e708-c0bb-4af5-af98-52a0e4e1d0d8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "7e7cf7d7-3e94-4901-a9aa-89b4aaac180b"
        },
        {
            "id": "ab0b2293-9823-48b9-bda1-fba38fcb9cd2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 6,
            "m_owner": "7e7cf7d7-3e94-4901-a9aa-89b4aaac180b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "bac4c612-b653-4391-ab80-e996952f43e9",
    "visible": true
}