{
    "id": "31f82bfb-ba7a-4e7f-84cd-e589a6d807b4",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_food",
    "eventList": [
        {
            "id": "87003ae4-40fe-452b-b441-bb9b61eb808a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "b146177e-0fec-4476-a28d-eede5309c867",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "31f82bfb-ba7a-4e7f-84cd-e589a6d807b4"
        },
        {
            "id": "5f2937ab-e4e0-4e56-9946-6f82887fc94f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "c94c581d-d573-47e5-ac7d-9aabf80084fb",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "31f82bfb-ba7a-4e7f-84cd-e589a6d807b4"
        },
        {
            "id": "cefa6988-48ee-4522-abef-ef3abd7148c1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "51751ed3-819a-4d30-859d-9d241b5dd289",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "31f82bfb-ba7a-4e7f-84cd-e589a6d807b4"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "3723db1e-b5e7-4756-8418-27a25cafb29b",
    "visible": true
}