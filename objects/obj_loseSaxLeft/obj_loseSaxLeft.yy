{
    "id": "2e916d9a-057b-41ed-a714-3e6d115e7203",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_loseSaxLeft",
    "eventList": [
        {
            "id": "80716d91-9fbe-40e6-b91e-8c17f6c616a3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "2e916d9a-057b-41ed-a714-3e6d115e7203"
        },
        {
            "id": "1bc16343-303a-460d-bc7f-1258ff496ed0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "2e916d9a-057b-41ed-a714-3e6d115e7203"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "cb7dddda-4d94-47fa-9c32-fecff94cb467",
    "visible": true
}