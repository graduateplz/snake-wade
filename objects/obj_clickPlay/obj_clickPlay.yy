{
    "id": "f655f7f5-c3fa-4188-b1e8-7e1346b626ae",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_clickPlay",
    "eventList": [
        {
            "id": "ad6eb2c0-95ea-4f8d-a161-0184e7fb15c7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f655f7f5-c3fa-4188-b1e8-7e1346b626ae"
        },
        {
            "id": "0379f9ba-363a-48bd-abcd-84daf2f96371",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 6,
            "m_owner": "f655f7f5-c3fa-4188-b1e8-7e1346b626ae"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "1777e609-faf1-4958-8164-904bbb4cf619",
    "visible": true
}