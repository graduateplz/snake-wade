{
    "id": "9bf4bf2e-4c53-42d4-ace1-d1688ac70c1d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obt_controlsImage",
    "eventList": [
        {
            "id": "dc0055b8-4ffe-42d6-a56b-5382d39b282a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 6,
            "m_owner": "9bf4bf2e-4c53-42d4-ace1-d1688ac70c1d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "589a7ea2-1f2d-46ff-b525-26f80c5caf62",
    "visible": true
}