{
    "id": "c94c581d-d573-47e5-ac7d-9aabf80084fb",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_headMedium",
    "eventList": [
        {
            "id": "f5cf5b38-3fb8-4b7f-9db7-5137e399cd4b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c94c581d-d573-47e5-ac7d-9aabf80084fb"
        },
        {
            "id": "d3725f08-e64b-4945-b18a-2197727da9f4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "c94c581d-d573-47e5-ac7d-9aabf80084fb"
        },
        {
            "id": "f54ec23c-678d-48aa-9744-83d0aeb6ad06",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "c94c581d-d573-47e5-ac7d-9aabf80084fb"
        },
        {
            "id": "aec751d8-91dd-4a69-8bd4-023543b75d58",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "c94c581d-d573-47e5-ac7d-9aabf80084fb"
        },
        {
            "id": "aeb844cd-0007-4475-bea7-0511960ebf2a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 38,
            "eventtype": 5,
            "m_owner": "c94c581d-d573-47e5-ac7d-9aabf80084fb"
        },
        {
            "id": "17e816bc-32b3-4486-bc14-222ea277bbc7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 40,
            "eventtype": 5,
            "m_owner": "c94c581d-d573-47e5-ac7d-9aabf80084fb"
        },
        {
            "id": "420e1d98-39d7-44a0-b8ac-ec104c386106",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 39,
            "eventtype": 5,
            "m_owner": "c94c581d-d573-47e5-ac7d-9aabf80084fb"
        },
        {
            "id": "fdac710b-1ef8-43cb-affa-d262de8a6314",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 37,
            "eventtype": 5,
            "m_owner": "c94c581d-d573-47e5-ac7d-9aabf80084fb"
        },
        {
            "id": "58db20be-cf76-4c2b-9276-f8f4284f35b0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 65,
            "eventtype": 5,
            "m_owner": "c94c581d-d573-47e5-ac7d-9aabf80084fb"
        },
        {
            "id": "71ed6a2c-01f5-4a96-8140-308101315baa",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 68,
            "eventtype": 5,
            "m_owner": "c94c581d-d573-47e5-ac7d-9aabf80084fb"
        },
        {
            "id": "901b315c-b7c8-440d-bea9-da8871718ea9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 83,
            "eventtype": 5,
            "m_owner": "c94c581d-d573-47e5-ac7d-9aabf80084fb"
        },
        {
            "id": "3d5402c7-1cc0-4efd-8b24-4cf63a18f679",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 87,
            "eventtype": 5,
            "m_owner": "c94c581d-d573-47e5-ac7d-9aabf80084fb"
        },
        {
            "id": "594f37a1-bec9-40e5-a6cd-93b8f4aed41f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "31f82bfb-ba7a-4e7f-84cd-e589a6d807b4",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "c94c581d-d573-47e5-ac7d-9aabf80084fb"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "693a15c9-df41-4b43-8c13-ef439c0f405f",
    "visible": true
}