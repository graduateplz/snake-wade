{
    "id": "e3d6eed8-d1a3-4ed2-825c-c28af72fa380",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_easyClick",
    "eventList": [
        {
            "id": "2f0abb4d-a760-4e4e-8451-7d88ddda6003",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e3d6eed8-d1a3-4ed2-825c-c28af72fa380"
        },
        {
            "id": "f4f1fc24-84f8-48c3-ac90-ac9710bcc95c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 6,
            "m_owner": "e3d6eed8-d1a3-4ed2-825c-c28af72fa380"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "5b5b2b36-d31a-4332-b04c-1239576ed210",
    "visible": true
}