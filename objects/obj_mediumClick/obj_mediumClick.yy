{
    "id": "38d52899-9d34-4a3a-a73c-7950c7beba77",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_mediumClick",
    "eventList": [
        {
            "id": "f4834e30-fc76-446e-af6e-395a2a8cc437",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "38d52899-9d34-4a3a-a73c-7950c7beba77"
        },
        {
            "id": "3e41f385-19a3-42fd-965d-6d4fea53a9cc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "38d52899-9d34-4a3a-a73c-7950c7beba77"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "326f5a80-59a7-4b4d-9d31-c2ac89a6f63a",
    "visible": true
}