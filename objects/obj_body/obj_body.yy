{
    "id": "f05e872b-a216-4510-9550-4805441c3774",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_body",
    "eventList": [
        {
            "id": "e1c18657-d417-4ce7-82ad-02096839aab1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "f05e872b-a216-4510-9550-4805441c3774"
        },
        {
            "id": "cd0c718a-0c3a-417e-bb95-89234c61c9b5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f05e872b-a216-4510-9550-4805441c3774"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "b1aed30c-41b6-42ce-9928-3d310ce7eb3e",
    "visible": true
}