{
    "id": "3a2fd00a-d7da-4e8a-9aab-f2ad9cfbd223",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_clickControls",
    "eventList": [
        {
            "id": "0549dccb-23ee-48d9-b9b2-e1a7139a1d62",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "3a2fd00a-d7da-4e8a-9aab-f2ad9cfbd223"
        },
        {
            "id": "80b16154-4ed3-4f89-9a97-72d08932849d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 6,
            "m_owner": "3a2fd00a-d7da-4e8a-9aab-f2ad9cfbd223"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "6b27a7fd-485e-4e01-b63a-c5411c185013",
    "visible": true
}