{
    "id": "51751ed3-819a-4d30-859d-9d241b5dd289",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_headEasy",
    "eventList": [
        {
            "id": "33389b0c-120b-4130-b404-70efe8bbd09b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "51751ed3-819a-4d30-859d-9d241b5dd289"
        },
        {
            "id": "43722501-b8b5-41d0-99c1-4a54cf0c67e9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "51751ed3-819a-4d30-859d-9d241b5dd289"
        },
        {
            "id": "b0bf21fe-ed94-4aaa-a090-786029f61636",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "51751ed3-819a-4d30-859d-9d241b5dd289"
        },
        {
            "id": "913bac07-fff8-4f22-acd8-78c8720ee809",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "51751ed3-819a-4d30-859d-9d241b5dd289"
        },
        {
            "id": "f81d52ec-cd3e-484b-8fee-6e09c24afc66",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 38,
            "eventtype": 5,
            "m_owner": "51751ed3-819a-4d30-859d-9d241b5dd289"
        },
        {
            "id": "9225801d-0752-46e9-88db-4ebe5e23a2b8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 40,
            "eventtype": 5,
            "m_owner": "51751ed3-819a-4d30-859d-9d241b5dd289"
        },
        {
            "id": "3ba3033b-7470-45c2-8057-ba1a818a0f35",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 39,
            "eventtype": 5,
            "m_owner": "51751ed3-819a-4d30-859d-9d241b5dd289"
        },
        {
            "id": "5aa3225f-a708-487e-bd5d-d49698f86ea5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 37,
            "eventtype": 5,
            "m_owner": "51751ed3-819a-4d30-859d-9d241b5dd289"
        },
        {
            "id": "3bea9f4c-f2e5-466d-9938-59d552385445",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 65,
            "eventtype": 5,
            "m_owner": "51751ed3-819a-4d30-859d-9d241b5dd289"
        },
        {
            "id": "91f0ff89-1883-4afe-bfd4-81c6f1b66ceb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 68,
            "eventtype": 5,
            "m_owner": "51751ed3-819a-4d30-859d-9d241b5dd289"
        },
        {
            "id": "dd108c4d-9511-4bac-9bae-ef739ceab323",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 83,
            "eventtype": 5,
            "m_owner": "51751ed3-819a-4d30-859d-9d241b5dd289"
        },
        {
            "id": "dd616525-537e-4351-97cb-c5a3771e4f93",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 87,
            "eventtype": 5,
            "m_owner": "51751ed3-819a-4d30-859d-9d241b5dd289"
        },
        {
            "id": "26e842f7-9814-412a-961c-fe3739116b81",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "31f82bfb-ba7a-4e7f-84cd-e589a6d807b4",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "51751ed3-819a-4d30-859d-9d241b5dd289"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "7bbf9a0c-6551-4712-b87d-678e80a0919a",
    "visible": true
}