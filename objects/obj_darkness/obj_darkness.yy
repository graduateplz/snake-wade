{
    "id": "90679799-5f2b-4480-ba9d-2bbea431e489",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_darkness",
    "eventList": [
        {
            "id": "8004edec-aa2c-48e8-94a4-86e5b2115b8f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "90679799-5f2b-4480-ba9d-2bbea431e489"
        },
        {
            "id": "ca4b0a6d-70fb-47cc-877d-7c444d86c147",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "90679799-5f2b-4480-ba9d-2bbea431e489"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "57eea600-08c7-4f4e-9b25-ee30a3d6bf35",
    "visible": true
}