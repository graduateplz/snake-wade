{
    "id": "b146177e-0fec-4476-a28d-eede5309c867",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_headHard",
    "eventList": [
        {
            "id": "935d7970-1258-43af-abfe-a209107a7958",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b146177e-0fec-4476-a28d-eede5309c867"
        },
        {
            "id": "a51ff9de-9224-4abe-8531-1f707dc90bd4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "b146177e-0fec-4476-a28d-eede5309c867"
        },
        {
            "id": "aa797ac6-caff-4e25-ac7f-efef4200ae9e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "b146177e-0fec-4476-a28d-eede5309c867"
        },
        {
            "id": "6a20424c-2c70-499a-924c-c52f57cee278",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "b146177e-0fec-4476-a28d-eede5309c867"
        },
        {
            "id": "511dd23c-5dd2-420d-a44a-e9bee2569094",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 38,
            "eventtype": 5,
            "m_owner": "b146177e-0fec-4476-a28d-eede5309c867"
        },
        {
            "id": "2ce86499-8d27-498a-a70d-4043bbbd2721",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 40,
            "eventtype": 5,
            "m_owner": "b146177e-0fec-4476-a28d-eede5309c867"
        },
        {
            "id": "39f3ecdd-e70c-4755-bf11-303fd31c2b46",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 39,
            "eventtype": 5,
            "m_owner": "b146177e-0fec-4476-a28d-eede5309c867"
        },
        {
            "id": "5a87d53b-e26a-46d9-bbf4-a72582aec159",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 37,
            "eventtype": 5,
            "m_owner": "b146177e-0fec-4476-a28d-eede5309c867"
        },
        {
            "id": "4f373f27-5331-4aa2-bd53-51214fbcb7db",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 87,
            "eventtype": 5,
            "m_owner": "b146177e-0fec-4476-a28d-eede5309c867"
        },
        {
            "id": "166d76b9-1e66-487b-8404-048aaf7c16d9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 65,
            "eventtype": 5,
            "m_owner": "b146177e-0fec-4476-a28d-eede5309c867"
        },
        {
            "id": "f9d25fd6-2257-4af0-9ef3-4509c1061d0b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 83,
            "eventtype": 5,
            "m_owner": "b146177e-0fec-4476-a28d-eede5309c867"
        },
        {
            "id": "ff8b93cc-f35c-4038-a78f-ec5aedb67b57",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 68,
            "eventtype": 5,
            "m_owner": "b146177e-0fec-4476-a28d-eede5309c867"
        },
        {
            "id": "38a1e7bd-915d-41cd-9319-c4e7cddce743",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "31f82bfb-ba7a-4e7f-84cd-e589a6d807b4",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "b146177e-0fec-4476-a28d-eede5309c867"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "c4684a63-e087-4f35-a1f3-99d18101fe61",
    "visible": true
}