{
    "id": "ccf031ac-39fe-4722-ba23-6780ade6f68c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_loseSaxRight",
    "eventList": [
        {
            "id": "d9deb120-47e2-4823-b3e9-5da7de30d3e1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "ccf031ac-39fe-4722-ba23-6780ade6f68c"
        },
        {
            "id": "7d5925e3-bf8b-4d21-b1ce-571d9bc8a6ce",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ccf031ac-39fe-4722-ba23-6780ade6f68c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "3937fb1f-f986-4b18-a8b4-12217fdae8cc",
    "visible": true
}