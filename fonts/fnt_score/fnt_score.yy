{
    "id": "3d08e2cd-1469-4647-974c-cb15a35ef85a",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt_score",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Verdana",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "c460a222-0246-4a29-92c6-b2382943557e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "c6c9b9e7-3cf7-4da3-9cfc-1f42110feab9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 19,
                "offset": 2,
                "shift": 6,
                "w": 3,
                "x": 219,
                "y": 44
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "3dba7519-499c-4913-910b-1c9c7244bf78",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 19,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 211,
                "y": 44
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "826712f8-cc24-4fa2-bd92-7ad497e37070",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 19,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 198,
                "y": 44
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "d6b9ee72-daa7-4786-82fb-f241f4fb9e1e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 19,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 187,
                "y": 44
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "c190234c-b477-4722-afce-f7c3719d1ff5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 19,
                "offset": 1,
                "shift": 17,
                "w": 16,
                "x": 169,
                "y": 44
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "76003813-a331-4a6f-aa31-c3fc86e13518",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 19,
                "offset": 0,
                "shift": 12,
                "w": 13,
                "x": 154,
                "y": 44
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "7a10ef1a-d33a-44a7-9162-46e26ce359dd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 19,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 149,
                "y": 44
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "bc284309-b415-4021-ae3a-3d699fde08c0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 19,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 141,
                "y": 44
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "c639533d-63b1-4ef5-a3f4-97d41a5f0ed6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 19,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 134,
                "y": 44
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "b736be2b-2f46-4a59-b135-5b7a10cee9a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 19,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 224,
                "y": 44
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "e78528e0-5cf7-450c-b899-0c051fd5aba7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 19,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 121,
                "y": 44
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "c3e6bfe0-43d7-487d-a6de-7beb61820994",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 19,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 104,
                "y": 44
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "74ba4f26-b40b-4ff9-ad1d-66239556d1b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 19,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 96,
                "y": 44
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "3a5ecace-c8f7-4736-ba9a-c8e5d97ad05a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 19,
                "offset": 1,
                "shift": 6,
                "w": 3,
                "x": 91,
                "y": 44
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "82000a78-3a9d-43fb-9917-2d0791e89667",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 19,
                "offset": -1,
                "shift": 7,
                "w": 8,
                "x": 81,
                "y": 44
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "73c308a0-deac-4912-8aee-f563954bfaea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 19,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 70,
                "y": 44
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "62d14ad8-ffad-4036-86d1-6cf0d15c12da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 19,
                "offset": 2,
                "shift": 10,
                "w": 7,
                "x": 61,
                "y": 44
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "bbc53ed4-cb7a-4405-bf8e-a9607cc0c2b1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 19,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 50,
                "y": 44
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "9614fa79-f5a3-4038-87ba-69de9db5fb72",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 19,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 40,
                "y": 44
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "3e448927-c2ec-4b72-800a-8e9223556d6c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 28,
                "y": 44
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "7686ce36-7494-4996-8128-f551395f7ed4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 19,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 110,
                "y": 44
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "90e17099-c447-47cf-b280-469f0dbd47df",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 19,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 234,
                "y": 44
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "a52c17cc-81ec-47b6-bbf8-c3e230f3bbeb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 19,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 2,
                "y": 65
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "a0ee0a51-51ab-4c73-86b2-8c3e4e76ef7a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 13,
                "y": 65
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "d8e7d0bc-4872-43c1-a03a-be757709f742",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 2,
                "y": 86
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "2c76ef72-023b-43c9-b6c8-0630ca037d26",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 19,
                "offset": 2,
                "shift": 7,
                "w": 3,
                "x": 242,
                "y": 65
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "db5add41-19f5-4c2e-ba68-feaecdc1d741",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 19,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 235,
                "y": 65
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "dc889d5a-cf2c-4124-b2c2-750689b87c4a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 19,
                "offset": 1,
                "shift": 13,
                "w": 10,
                "x": 223,
                "y": 65
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "55821dd3-2177-49fc-9287-06c908dbce18",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 19,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 210,
                "y": 65
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "1a938dc3-f18c-4f3c-a552-67d5cf5ae0ac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 19,
                "offset": 2,
                "shift": 13,
                "w": 10,
                "x": 198,
                "y": 65
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "944067f9-30fe-465d-9df9-eff511750eef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 189,
                "y": 65
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "a35ac5ac-d452-4be2-aecc-751f06df1fac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 19,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 173,
                "y": 65
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "08f89463-ca19-4f67-b853-6b010351c436",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 19,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 160,
                "y": 65
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "752b1798-e639-42c9-ba91-2b79d8be8ea0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 19,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 148,
                "y": 65
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "9904627b-402c-4677-9466-b69cc3efa7b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 19,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 135,
                "y": 65
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "132f6cba-c522-4408-ac61-010c23394ee1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 19,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 122,
                "y": 65
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "8e0989e0-80db-43b0-ab47-2e97f9929335",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 19,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 111,
                "y": 65
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "27e55afc-8312-4d62-9364-6d595789b775",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 101,
                "y": 65
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "afd94f6b-b3d2-4786-8d40-3ca0bef82d07",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 19,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 87,
                "y": 65
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "854a232c-a887-43f1-9b15-d3a625adc234",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 19,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 75,
                "y": 65
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "3649fb05-b989-48d3-b4f8-bcb3d4cb632a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 19,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 68,
                "y": 65
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "278e8b37-6d5b-47c7-b323-ca1e847aba85",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 60,
                "y": 65
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "3ab33ca6-7014-423c-9b2e-ef5775542312",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 19,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 48,
                "y": 65
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "407d02dd-4fdc-4912-b088-f06ed33e5088",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 38,
                "y": 65
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "b86fa2bc-ac8e-4c0a-9b53-16db1ff1e5e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 19,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 25,
                "y": 65
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "bb495b0f-5215-4fb1-ac17-324b19d44b08",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 19,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 16,
                "y": 44
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "ae7d8c5b-0eb9-46b6-b65c-37fe1f7884ea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 19,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 2,
                "y": 44
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "8cdd8f23-05fa-4546-af79-ffe773626104",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 19,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 243,
                "y": 23
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "6249b4b2-4ffd-4fdb-95f5-a5dfde656ef4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 19,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 2,
                "y": 23
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "c9d17229-ab7f-4ba6-b7f5-6efa98e61a1b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 19,
                "offset": 1,
                "shift": 11,
                "w": 11,
                "x": 228,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "752a30b6-2f88-4e17-92ec-78e0ded028d9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 19,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 216,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "719ba699-bf1f-4df0-95cf-74f007bc1b06",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 204,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "d430a631-8ad2-402e-ae84-d7821d18c38a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 19,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 192,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "469b6453-a6d6-4c48-8c52-41890c5eb54e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 19,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 179,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "47369d44-0890-4cd7-ab13-f66f68d473c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 19,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 161,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "0cd9dd8f-dc92-4fbd-ba9d-794ec7d1ab6d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 19,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 148,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "52f71678-32dd-4565-b95b-8bd37d8a9935",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 136,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "ce0325c9-b339-44d1-be56-22d6403a4b5a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 19,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 123,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "aeb232f5-c98b-477c-b907-fa849f083895",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 19,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 241,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "39428ff0-ec51-4dfe-8f25-5faa36608052",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 113,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "515dbd9d-ddc1-4837-b8e1-9ca02b0f2a22",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 19,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 97,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "85eea8fa-1fed-4423-a5ba-9484c9bfeeb8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 19,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 84,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "9ecc505d-2ea7-46a8-98d3-ba8b9d02d1c8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 19,
                "offset": -1,
                "shift": 10,
                "w": 12,
                "x": 70,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "e49cfc32-1ccb-4ab7-b859-e14968170812",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 19,
                "offset": 2,
                "shift": 10,
                "w": 4,
                "x": 64,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "968214d8-658a-427a-9264-a5b7243b7d87",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 53,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "9ea33566-3159-4364-b447-3a1dead30ebf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 19,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 42,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "7b1d012d-14a1-4c09-8b95-5a15fbab965d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 32,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "b26c871e-3d40-4617-ac36-b744b63b19af",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 21,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "fb1351a5-db37-445f-92ce-d8e00be9c836",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 10,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "c40dcf64-8685-4afe-91b5-bdb9d47dc28e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 7,
                "x": 104,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "233084ef-e4d0-411b-8e40-a1b4027ca353",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 16,
                "y": 23
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "0ed5b81f-fce3-4cb3-95b5-b518babd2061",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 19,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 125,
                "y": 23
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "c11f2269-302e-4dbe-bd50-d5808fa2895e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 19,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 27,
                "y": 23
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "d94e67f0-65b3-490e-bbb8-ef39384105c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 19,
                "offset": -1,
                "shift": 6,
                "w": 6,
                "x": 225,
                "y": 23
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "79561ca8-22c8-4ced-b477-27f927642517",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 214,
                "y": 23
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "281da11e-c80a-4908-856b-b1fdb93f7035",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 19,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 210,
                "y": 23
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "632ce288-501c-42a7-9e38-2ed9dbb01655",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 19,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 194,
                "y": 23
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "1ba2f6ea-adbf-4311-9442-944d8138248d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 19,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 184,
                "y": 23
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "240cde7b-4133-4efa-ad3d-a1ddd840cd11",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 173,
                "y": 23
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "6bd4db7e-bae8-45ab-9f16-8e4741c863fa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 19,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 162,
                "y": 23
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "ecda2bd8-6d42-4e66-8934-80581d93c942",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 151,
                "y": 23
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "5d9aed58-73db-4ba4-9e59-e7e5b1083dfb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 19,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 143,
                "y": 23
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "3eed832f-c7c9-494e-bbfc-241ea30deb10",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 233,
                "y": 23
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "c95830da-ece3-43b5-bee9-c21ca504e514",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 135,
                "y": 23
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "a19e7686-b616-43b0-a179-a3ff6e87a507",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 19,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 115,
                "y": 23
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "a14abffc-6d0a-4e60-b859-d6df7a440315",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 104,
                "y": 23
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "aa7fe34c-3355-44b4-b539-8a59c44f96a7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 19,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 89,
                "y": 23
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "2e519230-5f7c-4a67-9ef4-cc0cbe57d49a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 78,
                "y": 23
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "f2c2e4f6-e990-4df9-9370-f10a2ed93ab1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 67,
                "y": 23
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "43378928-5f66-45b3-90b7-8d822abf4648",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 57,
                "y": 23
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "d13d24bd-3cdd-4158-87d6-b6ddf7b2c748",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 19,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 47,
                "y": 23
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "36421cf2-a8ca-402a-a571-f07ba4e3e473",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 19,
                "offset": 2,
                "shift": 7,
                "w": 3,
                "x": 42,
                "y": 23
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "d708f3cf-c27d-4cfc-a77c-4d96e9ae4726",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 19,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 32,
                "y": 23
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "b9804939-c80e-47d2-b327-b06b3e72c017",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 19,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 14,
                "y": 86
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "27ea1bf2-784e-4b4a-a273-58c54ac55a67",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 19,
                "offset": 3,
                "shift": 16,
                "w": 10,
                "x": 27,
                "y": 86
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        {
            "id": "3928d075-d2b2-4a41-86a2-57a8919979e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 8217
        },
        {
            "id": "677c2799-1444-4c9e-a0cb-f3ad95c2c6ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 8221
        },
        {
            "id": "9397287a-86b7-452f-97f1-ad9d626729d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 84
        },
        {
            "id": "81002d41-50cc-42d4-acf2-0e7c20b685df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 89
        },
        {
            "id": "6db323de-77c9-4508-88b7-ff191f6e73cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 44
        },
        {
            "id": "1bf531b8-a555-40b7-9c5b-0fd373692d51",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 45
        },
        {
            "id": "ac650393-ee91-478a-a5f4-ed92abc7d582",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 173
        },
        {
            "id": "b8e9bf0e-7f93-49c7-82d4-ed4abde5d387",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 8211
        },
        {
            "id": "d1fbf3ef-030f-4e9f-837f-e9843065f111",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 8212
        },
        {
            "id": "34b3d6e2-5bd1-4b98-948f-b079911a9ed3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 8217
        },
        {
            "id": "94a3ccba-1393-4392-ab4c-d1a2fad1a7ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 8221
        },
        {
            "id": "a985ae6a-fb90-44ce-9cc8-edb14e015329",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 8212
        },
        {
            "id": "005a21fc-3851-4cd9-a9f6-7bd659a71514",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 84
        },
        {
            "id": "0be10f77-a66f-4726-9aa6-fb4c090daaa5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 8217
        },
        {
            "id": "c11c9aa0-002e-44dc-8eec-d64fb8de9fcf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 8221
        },
        {
            "id": "a94afec1-35c9-4500-99f0-83bcb1518947",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 44
        },
        {
            "id": "bb7ed22b-ae36-4f0b-bcb3-75843301d20c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 46
        },
        {
            "id": "02eab4d6-51d5-4f8b-8c1b-601b22ea0e2a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 198
        },
        {
            "id": "f395c786-2bf2-46f9-ad07-0a3f0d94efc9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 8230
        },
        {
            "id": "6c5cb4d2-9bdc-48f4-b815-8cbff068839a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 45
        },
        {
            "id": "632acb6c-9068-4630-a354-9315281561d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 173
        },
        {
            "id": "91a7c8a8-c2e9-451f-a8f4-5c7331686be7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 8211
        },
        {
            "id": "00c04a09-226e-44a0-8657-683257637624",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 8212
        },
        {
            "id": "28784b3a-2951-4833-9a3f-9f4fc0e61243",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 39
        },
        {
            "id": "afe9f31f-1094-468a-947b-155cedd08fcb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 45
        },
        {
            "id": "6ebbebec-e66e-4979-8d41-014f8967b960",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 76,
            "second": 74
        },
        {
            "id": "5c90c21e-b9d0-4ccd-9a83-98eef8bda36d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 84
        },
        {
            "id": "5e6f4de2-571b-49db-af87-215559691ba4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 86
        },
        {
            "id": "8fd12991-3fea-494b-8f2b-d668c7f94616",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 89
        },
        {
            "id": "d68b0f73-8dbc-4f9b-b93d-b65de005a206",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 118
        },
        {
            "id": "b8c0a5a9-ae5b-4c6e-a271-bfefab8959c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 121
        },
        {
            "id": "b225f5b0-80a2-4180-9181-84cfd892bbb2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 173
        },
        {
            "id": "aa93e9ff-ca8e-4510-96db-d5aa599527d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 8211
        },
        {
            "id": "4434a337-a883-46f0-a687-3181b42d5f99",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 8212
        },
        {
            "id": "65d22920-9919-4606-a3b8-4d13c49412fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 8217
        },
        {
            "id": "4fedfddb-eab1-471c-ada8-b30118d34421",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 8221
        },
        {
            "id": "a8b5e68a-f5d8-4d0f-828c-88f63d22e54d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 44
        },
        {
            "id": "d35f7bd2-ad62-42ac-8f6a-ab43796ed85d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 46
        },
        {
            "id": "077ebdcc-c0d8-4f36-98b9-98b0e8207b75",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 8230
        },
        {
            "id": "6708cefd-ff9f-467d-82b5-222eabd8e8f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 44
        },
        {
            "id": "f0403ddb-bee9-460c-907f-bfdb4bcc8516",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 45
        },
        {
            "id": "3c569430-a461-4152-8ab5-bfea5a2989e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 46
        },
        {
            "id": "f77a8176-7aab-496f-afc7-eff125be364e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 58
        },
        {
            "id": "e0145849-9bd0-4fec-9020-9f11d0b6a7ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 59
        },
        {
            "id": "8b88a1d9-84c9-4ceb-bf90-53c2a1ad40e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 65
        },
        {
            "id": "763e0214-f6db-467c-ae04-ef1584d663d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 97
        },
        {
            "id": "8ca5e80b-c868-426c-bb36-918f125c294d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 99
        },
        {
            "id": "7a480201-4120-4b8a-bc6a-307f0d979589",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 101
        },
        {
            "id": "8ee14448-b3fe-45a3-8b7b-21a8890bec14",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 103
        },
        {
            "id": "48fb72f2-5278-4945-a39e-92be7290baf6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 111
        },
        {
            "id": "c494f548-aa89-4a8d-a00e-9f621503726d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 114
        },
        {
            "id": "634c6bdf-ed9e-4c3d-bdb5-a3fe6e1e4380",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 115
        },
        {
            "id": "822791bb-ef4c-4dfc-91f5-178f21ca3577",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 117
        },
        {
            "id": "1e77e725-5f7f-44ff-9525-bc7efa16d611",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 118
        },
        {
            "id": "ec9678c7-4342-4a17-8e8b-4a6ae76b3a0f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 119
        },
        {
            "id": "7c9576ff-3c53-43ac-b549-1f0d77d478bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 121
        },
        {
            "id": "59cbc869-82e6-4652-a3d3-d0c92234baad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 122
        },
        {
            "id": "3be23474-d21a-4708-8c92-c8eefe3b88b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 173
        },
        {
            "id": "4da931ee-590b-45d5-911a-e55873a67f21",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 198
        },
        {
            "id": "bbdea05c-eef2-4c87-a0e4-a3e155e6c644",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 230
        },
        {
            "id": "63d6fc3c-3bdc-4f15-a172-225c790211c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 248
        },
        {
            "id": "82527c71-e8b5-47f5-8171-39b66c825c70",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 339
        },
        {
            "id": "46039056-0ee0-4e95-af0f-043f45fcfa5d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 894
        },
        {
            "id": "c51934aa-fd66-4339-8cdf-8c86566a74ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 8211
        },
        {
            "id": "b40e117d-29a0-48f8-9b87-e41104b4c597",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 8212
        },
        {
            "id": "1866704c-b3bd-4e47-b856-3aa062724978",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 8230
        },
        {
            "id": "eee909d4-58c7-4921-8b16-1ac53ba7aed6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 44
        },
        {
            "id": "bd370815-260f-4c38-8af7-86d040cb5690",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 46
        },
        {
            "id": "77a907f7-ddb1-44dd-af93-0ef3461c1be8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 8230
        },
        {
            "id": "07e3bdfb-cb02-4454-b3a1-2c2d5fc215b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 44
        },
        {
            "id": "a7d80159-65c1-48a1-9de1-8858a723c602",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 46
        },
        {
            "id": "781a1b9a-3ede-4114-ad58-5f849d065f2f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 8230
        },
        {
            "id": "95d32544-9736-4c63-b212-5f4db64275e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 44
        },
        {
            "id": "34609107-6be7-44de-bae2-78cf2f1b0d42",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 45
        },
        {
            "id": "aea42f2f-3c0d-4002-a023-ecca2359a6c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 46
        },
        {
            "id": "0d5bdf45-924c-444a-9586-779f39ab7087",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 58
        },
        {
            "id": "ae085cb7-6bfd-4207-bee4-89cda7df06f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 59
        },
        {
            "id": "e4e1c938-3e1d-4bce-b12d-8dd4559cf36a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 97
        },
        {
            "id": "ec3e157b-e447-45a0-ba65-8e17582a1e22",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 100
        },
        {
            "id": "b36d641e-c528-4aa6-a735-ada2b7958b23",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 101
        },
        {
            "id": "833ba7a9-0e83-4fc5-ae75-f5ec8d8bea79",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 103
        },
        {
            "id": "677bdbec-ebc6-40a8-ba42-76f3fabff67b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 111
        },
        {
            "id": "4faf4616-1861-4c35-bf85-78f150706b79",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 113
        },
        {
            "id": "c8dd11a8-eff2-4743-9853-bbaf79dccf63",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 115
        },
        {
            "id": "dc8e9d47-49f5-4c9c-893e-d6e247304704",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 117
        },
        {
            "id": "47b81047-5910-4098-bf3c-90200756ff26",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 173
        },
        {
            "id": "9ceadf33-f2c3-4851-b576-84e05d9dc70c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 230
        },
        {
            "id": "b2c1145f-f5c7-4888-b0a6-44af9b14cf48",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 248
        },
        {
            "id": "0dd6d678-eb93-4414-95c9-74ddd5194216",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 339
        },
        {
            "id": "e074a107-7a35-4c6b-87d6-36e782eba751",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 894
        },
        {
            "id": "cfc7ac8b-d55b-4deb-9c09-d1113942ec3a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 8211
        },
        {
            "id": "d5fb3b5d-5d39-41a7-8b63-a6cb8cff3411",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 8212
        },
        {
            "id": "2b22dc13-d25f-4b34-9a97-6c3c47118485",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 8230
        },
        {
            "id": "e718c10d-083e-4878-b861-844729a8642a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 84
        },
        {
            "id": "f82abc45-295f-4257-864f-1afa4e43176c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 41
        },
        {
            "id": "acdc30f8-a013-46ca-9307-bca433b99b27",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 44
        },
        {
            "id": "f9b719d9-788d-4cb4-afd3-2a90df25d016",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 46
        },
        {
            "id": "84863884-86e2-4d1a-9245-b245c290832a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 63
        },
        {
            "id": "f8f47a42-4c64-4754-9572-3cc173ef3692",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 92
        },
        {
            "id": "9950d5d3-a730-4d1c-9ffa-47002d25f502",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 93
        },
        {
            "id": "bf005bcf-eea6-41c5-bb84-29f1b0ff1b5d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 125
        },
        {
            "id": "f23c762f-2d61-4ede-b8bc-2ce8cd9df745",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 8230
        },
        {
            "id": "ed36fa1b-762a-483f-95b5-393bdf581ec8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 44
        },
        {
            "id": "58b93b95-02b4-451d-b544-b23b7c7490b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 46
        },
        {
            "id": "759fc579-8869-4d47-9fad-be73f0caf3c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 8230
        },
        {
            "id": "42f9ce2b-7b6b-4a06-a56b-44cdad0d88f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 44
        },
        {
            "id": "cf977a9c-93c4-4922-9a5e-9f7fc4685973",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 46
        },
        {
            "id": "669fa5b9-8635-4826-9925-efc3bdd0b562",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 8230
        },
        {
            "id": "8e328522-f1dd-4b3a-91fb-bff3c6a791fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 44
        },
        {
            "id": "83070c5d-d1a5-4eb5-9487-ddf6402ac3b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 46
        },
        {
            "id": "bd3fff9c-097d-4d36-ad24-0fd275e37b89",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 8230
        }
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 12,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}