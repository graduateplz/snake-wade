{
    "id": "1981fdd7-083f-46fd-ae76-7b9a29a8239d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_loseTile",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1299,
    "bbox_left": 0,
    "bbox_right": 1299,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "db46eab5-f9b5-4027-833e-788e7a9a7de4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1981fdd7-083f-46fd-ae76-7b9a29a8239d",
            "compositeImage": {
                "id": "cd584b6d-80d0-42eb-887f-16565239f33f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "db46eab5-f9b5-4027-833e-788e7a9a7de4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fd6cac00-430f-4842-a632-769a75f16920",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "db46eab5-f9b5-4027-833e-788e7a9a7de4",
                    "LayerId": "dae28212-db9f-4c5d-a436-e7eb5cfb991c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1300,
    "layers": [
        {
            "id": "dae28212-db9f-4c5d-a436-e7eb5cfb991c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1981fdd7-083f-46fd-ae76-7b9a29a8239d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1300,
    "xorig": 0,
    "yorig": 0
}