{
    "id": "693a15c9-df41-4b43-8c13-ef439c0f405f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_headMedium",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 28,
    "bbox_left": 3,
    "bbox_right": 28,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "63d55ce5-2df5-4825-8a10-8df16964e182",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "693a15c9-df41-4b43-8c13-ef439c0f405f",
            "compositeImage": {
                "id": "bc88dae9-96d0-4cf1-9941-acd2c4461426",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "63d55ce5-2df5-4825-8a10-8df16964e182",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef6a2436-2403-4af1-a71b-b6236b3b78b5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "63d55ce5-2df5-4825-8a10-8df16964e182",
                    "LayerId": "30611d59-720c-49b3-8e91-d82c8a08ccbd"
                }
            ]
        },
        {
            "id": "6508c05e-206f-43a6-9a58-7239f4f1d046",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "693a15c9-df41-4b43-8c13-ef439c0f405f",
            "compositeImage": {
                "id": "114d94d1-ecea-4766-8d69-59fd5524e94c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6508c05e-206f-43a6-9a58-7239f4f1d046",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6307d3ee-d979-4266-885f-ade1922b3760",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6508c05e-206f-43a6-9a58-7239f4f1d046",
                    "LayerId": "30611d59-720c-49b3-8e91-d82c8a08ccbd"
                }
            ]
        },
        {
            "id": "d5d2d6ea-de4f-4a45-aba2-e3dd12269dfb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "693a15c9-df41-4b43-8c13-ef439c0f405f",
            "compositeImage": {
                "id": "dc7657ad-1ed6-4470-97ae-750c8dc6c5a7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d5d2d6ea-de4f-4a45-aba2-e3dd12269dfb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dc427029-b28d-41ad-879d-bcd4cf4ca944",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d5d2d6ea-de4f-4a45-aba2-e3dd12269dfb",
                    "LayerId": "30611d59-720c-49b3-8e91-d82c8a08ccbd"
                }
            ]
        },
        {
            "id": "b5c4ccb1-c783-4ded-b779-089bdfaa8f16",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "693a15c9-df41-4b43-8c13-ef439c0f405f",
            "compositeImage": {
                "id": "72b7164d-0386-429c-8c0f-2fe1023d6644",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b5c4ccb1-c783-4ded-b779-089bdfaa8f16",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "95d72e52-0088-4f7f-af0b-56a0cb321e19",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b5c4ccb1-c783-4ded-b779-089bdfaa8f16",
                    "LayerId": "30611d59-720c-49b3-8e91-d82c8a08ccbd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "30611d59-720c-49b3-8e91-d82c8a08ccbd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "693a15c9-df41-4b43-8c13-ef439c0f405f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}