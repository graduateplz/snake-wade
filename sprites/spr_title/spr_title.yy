{
    "id": "e6362a0c-e569-48e8-a0a3-cd2638979f4b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_title",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 299,
    "bbox_left": 20,
    "bbox_right": 533,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7dbad6ad-7cee-4026-a0d6-1af857249e85",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e6362a0c-e569-48e8-a0a3-cd2638979f4b",
            "compositeImage": {
                "id": "720d4183-e9c1-43f2-b08c-b62e2cb0327c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7dbad6ad-7cee-4026-a0d6-1af857249e85",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "afbcd97e-4cb7-414f-910e-b9b603ab0947",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7dbad6ad-7cee-4026-a0d6-1af857249e85",
                    "LayerId": "fbbb8469-0b4a-4495-889c-e9736e0607cc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 300,
    "layers": [
        {
            "id": "fbbb8469-0b4a-4495-889c-e9736e0607cc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e6362a0c-e569-48e8-a0a3-cd2638979f4b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 600,
    "xorig": 300,
    "yorig": 150
}