{
    "id": "326f5a80-59a7-4b4d-9d31-c2ac89a6f63a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_mediumClick",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 69,
    "bbox_left": 0,
    "bbox_right": 69,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "013cd536-4dd4-4da0-9060-6b48588ebae6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "326f5a80-59a7-4b4d-9d31-c2ac89a6f63a",
            "compositeImage": {
                "id": "f648b292-e47c-4aba-ba39-4eb2072508c8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "013cd536-4dd4-4da0-9060-6b48588ebae6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5705a8c1-8d40-47da-a299-698553f55c05",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "013cd536-4dd4-4da0-9060-6b48588ebae6",
                    "LayerId": "cae9a1a8-ab32-4730-b9e9-fc4d57fa7a7b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 70,
    "layers": [
        {
            "id": "cae9a1a8-ab32-4730-b9e9-fc4d57fa7a7b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "326f5a80-59a7-4b4d-9d31-c2ac89a6f63a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 70,
    "xorig": 35,
    "yorig": 35
}