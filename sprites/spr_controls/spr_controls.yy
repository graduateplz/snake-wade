{
    "id": "6b27a7fd-485e-4e01-b63a-c5411c185013",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_controls",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 229,
    "bbox_left": 0,
    "bbox_right": 589,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0ea3fb01-d562-4e4b-888e-5b3f26f8de56",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6b27a7fd-485e-4e01-b63a-c5411c185013",
            "compositeImage": {
                "id": "5efb201d-95ca-46b9-9172-eb7e3e8d258a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0ea3fb01-d562-4e4b-888e-5b3f26f8de56",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8e3bef4f-b645-40e0-a17f-844fc5e2d9aa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0ea3fb01-d562-4e4b-888e-5b3f26f8de56",
                    "LayerId": "59f1c559-d1b0-4a6a-9cf5-a7bcfb0db618"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 230,
    "layers": [
        {
            "id": "59f1c559-d1b0-4a6a-9cf5-a7bcfb0db618",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6b27a7fd-485e-4e01-b63a-c5411c185013",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 590,
    "xorig": 0,
    "yorig": 0
}