{
    "id": "fbe57433-c13d-47c3-aa3d-6d1228c4dad5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_controlsTile",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1299,
    "bbox_left": 0,
    "bbox_right": 1299,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5bcf679c-c1ae-4202-8d4a-26a6f21ff612",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fbe57433-c13d-47c3-aa3d-6d1228c4dad5",
            "compositeImage": {
                "id": "a0b90c55-b2d3-4b38-9510-0d3aab20344b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5bcf679c-c1ae-4202-8d4a-26a6f21ff612",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "34f34f7f-b316-49ea-acb7-3cd036853aa5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5bcf679c-c1ae-4202-8d4a-26a6f21ff612",
                    "LayerId": "195f7ff5-bea7-466d-b8b1-a2abf95a037d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1300,
    "layers": [
        {
            "id": "195f7ff5-bea7-466d-b8b1-a2abf95a037d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fbe57433-c13d-47c3-aa3d-6d1228c4dad5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1300,
    "xorig": 0,
    "yorig": 0
}