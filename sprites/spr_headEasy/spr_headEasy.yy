{
    "id": "7bbf9a0c-6551-4712-b87d-678e80a0919a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_headEasy",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 1,
    "bbox_right": 31,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b8306e86-3548-40b5-a35b-168c45bb92ce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7bbf9a0c-6551-4712-b87d-678e80a0919a",
            "compositeImage": {
                "id": "63fe9795-ef01-4ed4-bc8e-24bd5b9a80f9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b8306e86-3548-40b5-a35b-168c45bb92ce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4535c278-fea1-49ce-9420-83ba06a18f9e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b8306e86-3548-40b5-a35b-168c45bb92ce",
                    "LayerId": "f64b1c20-7bd9-4d23-86ef-01aed9e6640b"
                }
            ]
        },
        {
            "id": "dc41b758-c00e-4c2c-b7ca-82a43c41c120",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7bbf9a0c-6551-4712-b87d-678e80a0919a",
            "compositeImage": {
                "id": "2a113df8-3f17-4d01-b18c-4d0bbc936e10",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dc41b758-c00e-4c2c-b7ca-82a43c41c120",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1d69f547-ff16-4ed0-96ca-5365eebc0d5b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dc41b758-c00e-4c2c-b7ca-82a43c41c120",
                    "LayerId": "f64b1c20-7bd9-4d23-86ef-01aed9e6640b"
                }
            ]
        },
        {
            "id": "8d0c5011-ccf2-424e-b613-395dbcb5ea2b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7bbf9a0c-6551-4712-b87d-678e80a0919a",
            "compositeImage": {
                "id": "d86d45db-5e67-430d-aa9c-a54cbae7557b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8d0c5011-ccf2-424e-b613-395dbcb5ea2b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b3318a6f-1bf1-442d-8d6b-2f2554879c96",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8d0c5011-ccf2-424e-b613-395dbcb5ea2b",
                    "LayerId": "f64b1c20-7bd9-4d23-86ef-01aed9e6640b"
                }
            ]
        },
        {
            "id": "bdf8ccd6-25a4-4d03-ae8b-e9919b919080",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7bbf9a0c-6551-4712-b87d-678e80a0919a",
            "compositeImage": {
                "id": "3b5d296f-45ca-4e21-bfa6-d9aff2551f0d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bdf8ccd6-25a4-4d03-ae8b-e9919b919080",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2526494f-3201-4cd7-b41b-c8a276985405",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bdf8ccd6-25a4-4d03-ae8b-e9919b919080",
                    "LayerId": "f64b1c20-7bd9-4d23-86ef-01aed9e6640b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "f64b1c20-7bd9-4d23-86ef-01aed9e6640b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7bbf9a0c-6551-4712-b87d-678e80a0919a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}