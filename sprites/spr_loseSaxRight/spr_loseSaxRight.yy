{
    "id": "3937fb1f-f986-4b18-a8b4-12217fdae8cc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_loseSaxRight",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 484,
    "bbox_left": 0,
    "bbox_right": 390,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "680e0085-66a5-4fad-a64a-e0c7e8cea51f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3937fb1f-f986-4b18-a8b4-12217fdae8cc",
            "compositeImage": {
                "id": "59724d5b-7967-4d1b-accc-5b5d18fa4396",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "680e0085-66a5-4fad-a64a-e0c7e8cea51f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5ba74ef7-742a-4686-8196-1abcee272b61",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "680e0085-66a5-4fad-a64a-e0c7e8cea51f",
                    "LayerId": "856a73fb-1f87-48e0-8e17-640055577f2b"
                }
            ]
        },
        {
            "id": "06efae55-f9a6-4e82-a573-9fa8314ba8c8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3937fb1f-f986-4b18-a8b4-12217fdae8cc",
            "compositeImage": {
                "id": "106c1a41-4875-42e4-b8b8-96ede1ca9784",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "06efae55-f9a6-4e82-a573-9fa8314ba8c8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d2d6c7a8-54fd-4b8b-a33e-58267d79a37e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "06efae55-f9a6-4e82-a573-9fa8314ba8c8",
                    "LayerId": "856a73fb-1f87-48e0-8e17-640055577f2b"
                }
            ]
        },
        {
            "id": "b551a046-712f-47f4-ac64-64285d493091",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3937fb1f-f986-4b18-a8b4-12217fdae8cc",
            "compositeImage": {
                "id": "4810b699-77cc-4fa3-8aca-bba7b33cb40d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b551a046-712f-47f4-ac64-64285d493091",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9e80720c-4e35-480a-8801-b7d55cea1736",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b551a046-712f-47f4-ac64-64285d493091",
                    "LayerId": "856a73fb-1f87-48e0-8e17-640055577f2b"
                }
            ]
        },
        {
            "id": "1c2c5f18-08f8-417e-a55c-5e3d7313cd59",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3937fb1f-f986-4b18-a8b4-12217fdae8cc",
            "compositeImage": {
                "id": "f9db1dcb-df0e-4d08-be30-24a7eee377b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1c2c5f18-08f8-417e-a55c-5e3d7313cd59",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9aea7357-f427-4d09-99bb-4d86b07bcea6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1c2c5f18-08f8-417e-a55c-5e3d7313cd59",
                    "LayerId": "856a73fb-1f87-48e0-8e17-640055577f2b"
                }
            ]
        },
        {
            "id": "e9e60680-5f24-4199-91b0-4ed5b6ebd22e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3937fb1f-f986-4b18-a8b4-12217fdae8cc",
            "compositeImage": {
                "id": "9bc3d853-318e-45df-a43a-64fc3377e6d6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e9e60680-5f24-4199-91b0-4ed5b6ebd22e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "24dff4f5-1922-4481-8401-9f3ae72243fd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e9e60680-5f24-4199-91b0-4ed5b6ebd22e",
                    "LayerId": "856a73fb-1f87-48e0-8e17-640055577f2b"
                }
            ]
        },
        {
            "id": "a2e7e0d7-19de-4b6f-bca2-c6e797bddc16",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3937fb1f-f986-4b18-a8b4-12217fdae8cc",
            "compositeImage": {
                "id": "dcc08fde-05b0-4d3f-9993-97a5862d47bb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a2e7e0d7-19de-4b6f-bca2-c6e797bddc16",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d102a1f1-c90e-462c-9c43-5a79f56b7d37",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a2e7e0d7-19de-4b6f-bca2-c6e797bddc16",
                    "LayerId": "856a73fb-1f87-48e0-8e17-640055577f2b"
                }
            ]
        },
        {
            "id": "56bdca2c-d2ec-4f13-b8ad-d0970e3d4b47",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3937fb1f-f986-4b18-a8b4-12217fdae8cc",
            "compositeImage": {
                "id": "0ed1a88e-7b6b-4688-9f55-2d12f4499c9a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "56bdca2c-d2ec-4f13-b8ad-d0970e3d4b47",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0067d5a5-39ed-431b-8833-ee657fbe5356",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "56bdca2c-d2ec-4f13-b8ad-d0970e3d4b47",
                    "LayerId": "856a73fb-1f87-48e0-8e17-640055577f2b"
                }
            ]
        },
        {
            "id": "51b81688-8f7c-4661-9d93-e8f41dbe0413",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3937fb1f-f986-4b18-a8b4-12217fdae8cc",
            "compositeImage": {
                "id": "e1690922-7009-4e4a-bfb2-79c846383a98",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "51b81688-8f7c-4661-9d93-e8f41dbe0413",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "290da575-7864-4148-b3ee-a3d16135475b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "51b81688-8f7c-4661-9d93-e8f41dbe0413",
                    "LayerId": "856a73fb-1f87-48e0-8e17-640055577f2b"
                }
            ]
        },
        {
            "id": "6dc8d7e3-1fa7-40fa-85c0-52a3eb95b53a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3937fb1f-f986-4b18-a8b4-12217fdae8cc",
            "compositeImage": {
                "id": "53ea5a36-7f76-4119-a923-45baa4e3b70b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6dc8d7e3-1fa7-40fa-85c0-52a3eb95b53a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c9fb5296-e6ad-44da-abb6-c37bfe9cab25",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6dc8d7e3-1fa7-40fa-85c0-52a3eb95b53a",
                    "LayerId": "856a73fb-1f87-48e0-8e17-640055577f2b"
                }
            ]
        },
        {
            "id": "4e494cf6-6ed5-4719-9f44-2ba4cb1ea249",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3937fb1f-f986-4b18-a8b4-12217fdae8cc",
            "compositeImage": {
                "id": "3845d895-6731-4beb-8317-c429b6847da9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4e494cf6-6ed5-4719-9f44-2ba4cb1ea249",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d7483522-1fb4-45d2-aabd-78d6176d6105",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4e494cf6-6ed5-4719-9f44-2ba4cb1ea249",
                    "LayerId": "856a73fb-1f87-48e0-8e17-640055577f2b"
                }
            ]
        },
        {
            "id": "ec4fe003-1d14-414b-bfb5-8950ea68f7a8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3937fb1f-f986-4b18-a8b4-12217fdae8cc",
            "compositeImage": {
                "id": "47756dd0-c38e-42de-b5e8-110450b824f3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ec4fe003-1d14-414b-bfb5-8950ea68f7a8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c356cc2f-f3b8-45cc-85d5-c99ceee6e209",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ec4fe003-1d14-414b-bfb5-8950ea68f7a8",
                    "LayerId": "856a73fb-1f87-48e0-8e17-640055577f2b"
                }
            ]
        },
        {
            "id": "31482cef-8e03-45a4-9592-7ae52db5976c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3937fb1f-f986-4b18-a8b4-12217fdae8cc",
            "compositeImage": {
                "id": "da3cab78-3466-4f08-ba58-9450db62e133",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "31482cef-8e03-45a4-9592-7ae52db5976c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3ef87589-2ba2-48bf-8c9d-6003f4bdbf9c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "31482cef-8e03-45a4-9592-7ae52db5976c",
                    "LayerId": "856a73fb-1f87-48e0-8e17-640055577f2b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 485,
    "layers": [
        {
            "id": "856a73fb-1f87-48e0-8e17-640055577f2b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3937fb1f-f986-4b18-a8b4-12217fdae8cc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 25,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 392,
    "xorig": 0,
    "yorig": 0
}