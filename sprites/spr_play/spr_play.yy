{
    "id": "1777e609-faf1-4958-8164-904bbb4cf619",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_play",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 229,
    "bbox_left": 0,
    "bbox_right": 589,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "177cc025-6cff-4fe8-bb4d-e7643c9ec159",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1777e609-faf1-4958-8164-904bbb4cf619",
            "compositeImage": {
                "id": "b560415b-0e35-4599-a3cb-b867d90fe19f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "177cc025-6cff-4fe8-bb4d-e7643c9ec159",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a5d59001-c755-47fe-8f3e-91dc2dd9f027",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "177cc025-6cff-4fe8-bb4d-e7643c9ec159",
                    "LayerId": "94df1f97-1982-4c5d-9bdb-8e11e7abb42b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 230,
    "layers": [
        {
            "id": "94df1f97-1982-4c5d-9bdb-8e11e7abb42b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1777e609-faf1-4958-8164-904bbb4cf619",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 590,
    "xorig": 0,
    "yorig": 0
}