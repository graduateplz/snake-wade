{
    "id": "b1aed30c-41b6-42ce-9928-3d310ce7eb3e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_body",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 27,
    "bbox_left": 2,
    "bbox_right": 27,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ccb5e11a-7bd5-4160-a65d-e3fc552b9f29",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b1aed30c-41b6-42ce-9928-3d310ce7eb3e",
            "compositeImage": {
                "id": "ed80f532-c1b5-44a1-8d13-34af35d86407",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ccb5e11a-7bd5-4160-a65d-e3fc552b9f29",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "56f0d91d-ce47-470c-be6c-b29b551aec4a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ccb5e11a-7bd5-4160-a65d-e3fc552b9f29",
                    "LayerId": "2378a471-eec2-4b85-87b8-f6dd8c5b86bf"
                }
            ]
        },
        {
            "id": "f047b196-9e44-4ffa-a8b4-8966091318fb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b1aed30c-41b6-42ce-9928-3d310ce7eb3e",
            "compositeImage": {
                "id": "a810b136-e110-4b82-a507-bdd2aa19d112",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f047b196-9e44-4ffa-a8b4-8966091318fb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4174a8f4-79e7-4cde-94f4-fdc42f23e812",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f047b196-9e44-4ffa-a8b4-8966091318fb",
                    "LayerId": "2378a471-eec2-4b85-87b8-f6dd8c5b86bf"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "2378a471-eec2-4b85-87b8-f6dd8c5b86bf",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b1aed30c-41b6-42ce-9928-3d310ce7eb3e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}