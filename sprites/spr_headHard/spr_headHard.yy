{
    "id": "c4684a63-e087-4f35-a1f3-99d18101fe61",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_headHard",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 26,
    "bbox_left": 5,
    "bbox_right": 26,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3bf5f3ef-0db4-49b1-b885-d7a80324526e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c4684a63-e087-4f35-a1f3-99d18101fe61",
            "compositeImage": {
                "id": "b00155f4-f817-4cf5-8edc-72a00dce76d1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3bf5f3ef-0db4-49b1-b885-d7a80324526e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4c25ec22-512d-4474-a8f2-8966837aed3f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3bf5f3ef-0db4-49b1-b885-d7a80324526e",
                    "LayerId": "5abd9c44-4719-4065-bb0d-030800ff61a2"
                }
            ]
        },
        {
            "id": "ecd1f4c6-7d8b-42a6-a235-a8914f0c0111",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c4684a63-e087-4f35-a1f3-99d18101fe61",
            "compositeImage": {
                "id": "500d9df0-cb0d-4b97-8945-edd4d96bf4cf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ecd1f4c6-7d8b-42a6-a235-a8914f0c0111",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3c5b7a19-073e-4504-8849-41bf97e6f488",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ecd1f4c6-7d8b-42a6-a235-a8914f0c0111",
                    "LayerId": "5abd9c44-4719-4065-bb0d-030800ff61a2"
                }
            ]
        },
        {
            "id": "55fb4815-4a98-4ba5-8dab-37f612f2258f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c4684a63-e087-4f35-a1f3-99d18101fe61",
            "compositeImage": {
                "id": "39168fea-4708-4340-a448-baf582a269ec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "55fb4815-4a98-4ba5-8dab-37f612f2258f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cffd8540-6fe6-4bcb-acc1-532df72628a5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "55fb4815-4a98-4ba5-8dab-37f612f2258f",
                    "LayerId": "5abd9c44-4719-4065-bb0d-030800ff61a2"
                }
            ]
        },
        {
            "id": "cb11e430-d542-4fe9-b470-c6e0371335c5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c4684a63-e087-4f35-a1f3-99d18101fe61",
            "compositeImage": {
                "id": "fcdec061-20cb-4a47-afd3-f0ad6f9ce484",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cb11e430-d542-4fe9-b470-c6e0371335c5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "af14e0f8-b6da-4eec-b56d-066363de812e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cb11e430-d542-4fe9-b470-c6e0371335c5",
                    "LayerId": "5abd9c44-4719-4065-bb0d-030800ff61a2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "5abd9c44-4719-4065-bb0d-030800ff61a2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c4684a63-e087-4f35-a1f3-99d18101fe61",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}