{
    "id": "9db225c4-3e17-4cfc-b446-c082466b57dd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_titleTile",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1299,
    "bbox_left": 0,
    "bbox_right": 1299,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3d776ffb-694e-4da6-9e9f-f0770c7c2e77",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9db225c4-3e17-4cfc-b446-c082466b57dd",
            "compositeImage": {
                "id": "41a8fc5e-70e4-4ae1-b2b2-d4a07b31fdf0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3d776ffb-694e-4da6-9e9f-f0770c7c2e77",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d5eaff04-ccf8-4c7f-b8d1-22afd2cbefff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3d776ffb-694e-4da6-9e9f-f0770c7c2e77",
                    "LayerId": "f401f506-7b3b-48e0-aac4-d5e64ead5e73"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1300,
    "layers": [
        {
            "id": "f401f506-7b3b-48e0-aac4-d5e64ead5e73",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9db225c4-3e17-4cfc-b446-c082466b57dd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1300,
    "xorig": 0,
    "yorig": 0
}