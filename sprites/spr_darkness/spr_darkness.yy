{
    "id": "57eea600-08c7-4f4e-9b25-ee30a3d6bf35",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_darkness",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1299,
    "bbox_left": 0,
    "bbox_right": 1299,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c0d8c6b8-c944-44fa-8adf-23ec799c299d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "57eea600-08c7-4f4e-9b25-ee30a3d6bf35",
            "compositeImage": {
                "id": "dc2c7db3-9324-42fa-89ad-59ac30a27325",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c0d8c6b8-c944-44fa-8adf-23ec799c299d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "51455fb6-0f5d-4c70-a130-dc290c291f24",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c0d8c6b8-c944-44fa-8adf-23ec799c299d",
                    "LayerId": "144275ef-37dd-4966-8762-b5d4d48e6e1c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1300,
    "layers": [
        {
            "id": "144275ef-37dd-4966-8762-b5d4d48e6e1c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "57eea600-08c7-4f4e-9b25-ee30a3d6bf35",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1300,
    "xorig": 0,
    "yorig": 0
}