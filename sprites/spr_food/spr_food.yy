{
    "id": "3723db1e-b5e7-4756-8418-27a25cafb29b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_food",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "52711365-a48d-48b1-9b7e-f802ecbeb142",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3723db1e-b5e7-4756-8418-27a25cafb29b",
            "compositeImage": {
                "id": "b2e94818-0668-4eae-91e5-1e7f9cbb2c7c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "52711365-a48d-48b1-9b7e-f802ecbeb142",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f0d61db7-c538-40b8-9639-b3506b9bb899",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "52711365-a48d-48b1-9b7e-f802ecbeb142",
                    "LayerId": "4e2392fc-1035-42b5-b60d-32abc65ca712"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "4e2392fc-1035-42b5-b60d-32abc65ca712",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3723db1e-b5e7-4756-8418-27a25cafb29b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}