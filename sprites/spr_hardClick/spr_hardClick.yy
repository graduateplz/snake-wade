{
    "id": "bac4c612-b653-4391-ab80-e996952f43e9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hardClick",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 69,
    "bbox_left": 0,
    "bbox_right": 69,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "080368a3-c7bd-4a68-9063-b40dec9b673e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bac4c612-b653-4391-ab80-e996952f43e9",
            "compositeImage": {
                "id": "6df50696-7bb3-4b8b-9c56-caeca7a918a3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "080368a3-c7bd-4a68-9063-b40dec9b673e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "52570634-c211-46c1-af86-4122472d5027",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "080368a3-c7bd-4a68-9063-b40dec9b673e",
                    "LayerId": "fd7abb1c-740f-46c4-8d16-4ae1f57274f1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 70,
    "layers": [
        {
            "id": "fd7abb1c-740f-46c4-8d16-4ae1f57274f1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bac4c612-b653-4391-ab80-e996952f43e9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 70,
    "xorig": 35,
    "yorig": 35
}