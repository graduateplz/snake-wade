{
    "id": "3e82eb9c-9a3a-4a1b-a5f6-0252184a7340",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_tail",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 28,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "694c88e9-fd0e-4d5d-ad11-d2d397a1086c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e82eb9c-9a3a-4a1b-a5f6-0252184a7340",
            "compositeImage": {
                "id": "1322f1ff-99d4-48bb-88e3-255e31b2c9b6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "694c88e9-fd0e-4d5d-ad11-d2d397a1086c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "43187905-f2b0-49eb-aec7-27e5c5b1760f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "694c88e9-fd0e-4d5d-ad11-d2d397a1086c",
                    "LayerId": "05c68456-1362-4669-b4bf-054028894540"
                }
            ]
        },
        {
            "id": "9e2359b3-da75-4419-ac08-4075354cac9d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e82eb9c-9a3a-4a1b-a5f6-0252184a7340",
            "compositeImage": {
                "id": "5e01113d-aae9-4ac2-902c-e3d5d3cd6d64",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9e2359b3-da75-4419-ac08-4075354cac9d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9c3c11fa-68e4-4abc-bcc1-fb0817cd6f69",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9e2359b3-da75-4419-ac08-4075354cac9d",
                    "LayerId": "05c68456-1362-4669-b4bf-054028894540"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 29,
    "layers": [
        {
            "id": "05c68456-1362-4669-b4bf-054028894540",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3e82eb9c-9a3a-4a1b-a5f6-0252184a7340",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}