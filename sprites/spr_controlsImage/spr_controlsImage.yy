{
    "id": "589a7ea2-1f2d-46ff-b525-26f80c5caf62",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_controlsImage",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 718,
    "bbox_left": 27,
    "bbox_right": 1002,
    "bbox_top": 14,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "265608c6-7aae-49b2-b77e-56921dd152b0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "589a7ea2-1f2d-46ff-b525-26f80c5caf62",
            "compositeImage": {
                "id": "3910dfa0-0a68-46c3-94e0-9a08812fe88f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "265608c6-7aae-49b2-b77e-56921dd152b0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "82c9811d-2fa0-4364-af14-7b00c631c8c8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "265608c6-7aae-49b2-b77e-56921dd152b0",
                    "LayerId": "29ae5a69-d302-4de3-b1ea-e82581bd4de2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 768,
    "layers": [
        {
            "id": "29ae5a69-d302-4de3-b1ea-e82581bd4de2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "589a7ea2-1f2d-46ff-b525-26f80c5caf62",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1024,
    "xorig": 0,
    "yorig": 0
}