{
    "id": "cb7dddda-4d94-47fa-9c32-fecff94cb467",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_loseSaxLeft",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 484,
    "bbox_left": 1,
    "bbox_right": 391,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "df32633e-ccb7-4221-875b-03d10c9951fc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cb7dddda-4d94-47fa-9c32-fecff94cb467",
            "compositeImage": {
                "id": "950bd035-0014-4eca-bf92-0ade34704ca1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "df32633e-ccb7-4221-875b-03d10c9951fc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1b4cba6d-3fe9-4523-836c-f857dd8653ac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "df32633e-ccb7-4221-875b-03d10c9951fc",
                    "LayerId": "3b6e9104-023c-4e51-af64-bdcec9a05811"
                }
            ]
        },
        {
            "id": "1623353a-f3aa-4e93-9b0e-fc99fbbe9456",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cb7dddda-4d94-47fa-9c32-fecff94cb467",
            "compositeImage": {
                "id": "0647922d-e2ad-4415-a1a5-2c724faca7ea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1623353a-f3aa-4e93-9b0e-fc99fbbe9456",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c2ff3f36-3e6b-4468-a96c-fb0c73236736",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1623353a-f3aa-4e93-9b0e-fc99fbbe9456",
                    "LayerId": "3b6e9104-023c-4e51-af64-bdcec9a05811"
                }
            ]
        },
        {
            "id": "c59c2bb4-4845-4458-b856-54852788fd83",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cb7dddda-4d94-47fa-9c32-fecff94cb467",
            "compositeImage": {
                "id": "b5ef54f8-844c-4a57-8b54-e6a1f32e6a3d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c59c2bb4-4845-4458-b856-54852788fd83",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a02d5857-4de8-429d-b979-02ec4dfca580",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c59c2bb4-4845-4458-b856-54852788fd83",
                    "LayerId": "3b6e9104-023c-4e51-af64-bdcec9a05811"
                }
            ]
        },
        {
            "id": "5442c371-82ce-4466-8046-5802a0ab0dc0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cb7dddda-4d94-47fa-9c32-fecff94cb467",
            "compositeImage": {
                "id": "12e88541-9463-4685-a420-6ead37f778c4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5442c371-82ce-4466-8046-5802a0ab0dc0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d4d325b0-0fa4-41a9-bbe0-9ed5743a9e70",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5442c371-82ce-4466-8046-5802a0ab0dc0",
                    "LayerId": "3b6e9104-023c-4e51-af64-bdcec9a05811"
                }
            ]
        },
        {
            "id": "50d4222d-af3f-4dbf-9f09-6be50f06f5eb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cb7dddda-4d94-47fa-9c32-fecff94cb467",
            "compositeImage": {
                "id": "9957c00a-f09c-4103-a0a8-51005679125f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "50d4222d-af3f-4dbf-9f09-6be50f06f5eb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a895c172-a412-4fe7-82df-b8db183f856c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "50d4222d-af3f-4dbf-9f09-6be50f06f5eb",
                    "LayerId": "3b6e9104-023c-4e51-af64-bdcec9a05811"
                }
            ]
        },
        {
            "id": "c3a42283-dd68-4491-a32f-8c535cb97ccb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cb7dddda-4d94-47fa-9c32-fecff94cb467",
            "compositeImage": {
                "id": "f1952d9a-b9de-495e-aa87-635d4499f810",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c3a42283-dd68-4491-a32f-8c535cb97ccb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2ad45821-9675-41ea-9773-3fba75973334",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c3a42283-dd68-4491-a32f-8c535cb97ccb",
                    "LayerId": "3b6e9104-023c-4e51-af64-bdcec9a05811"
                }
            ]
        },
        {
            "id": "30fd22cb-1ed1-49ff-b2d0-3eec1f864c02",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cb7dddda-4d94-47fa-9c32-fecff94cb467",
            "compositeImage": {
                "id": "76269925-f599-486d-be13-8703947e44fb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "30fd22cb-1ed1-49ff-b2d0-3eec1f864c02",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bdf3c9df-e9db-4c61-bc5b-cc47b07af3f6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "30fd22cb-1ed1-49ff-b2d0-3eec1f864c02",
                    "LayerId": "3b6e9104-023c-4e51-af64-bdcec9a05811"
                }
            ]
        },
        {
            "id": "47461b86-c60d-4d5a-aea1-69820906c0a8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cb7dddda-4d94-47fa-9c32-fecff94cb467",
            "compositeImage": {
                "id": "b0452917-1e53-4f08-b51a-544b1ee11208",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "47461b86-c60d-4d5a-aea1-69820906c0a8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0438a6a6-ecec-42f1-a01e-c22011f8d11d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "47461b86-c60d-4d5a-aea1-69820906c0a8",
                    "LayerId": "3b6e9104-023c-4e51-af64-bdcec9a05811"
                }
            ]
        },
        {
            "id": "c90d5ed2-0ed7-4f1d-83e8-fe455a9c5b9d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cb7dddda-4d94-47fa-9c32-fecff94cb467",
            "compositeImage": {
                "id": "43b39e7b-301d-48cc-b4ab-038db728507e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c90d5ed2-0ed7-4f1d-83e8-fe455a9c5b9d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c1c42d0b-7122-49ed-898d-3deca2d93de4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c90d5ed2-0ed7-4f1d-83e8-fe455a9c5b9d",
                    "LayerId": "3b6e9104-023c-4e51-af64-bdcec9a05811"
                }
            ]
        },
        {
            "id": "f830ac94-7554-43d3-b62a-b255279afc27",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cb7dddda-4d94-47fa-9c32-fecff94cb467",
            "compositeImage": {
                "id": "082834f6-49fc-49cf-81f4-2a8c2e1d53dc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f830ac94-7554-43d3-b62a-b255279afc27",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "74319086-ee2f-48a4-a3ef-33c18168f9f4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f830ac94-7554-43d3-b62a-b255279afc27",
                    "LayerId": "3b6e9104-023c-4e51-af64-bdcec9a05811"
                }
            ]
        },
        {
            "id": "2940c2d9-7755-4dd4-80f5-e6fe8d3614f2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cb7dddda-4d94-47fa-9c32-fecff94cb467",
            "compositeImage": {
                "id": "8d269c1d-55f6-421c-ae1b-ca8502f8a3d9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2940c2d9-7755-4dd4-80f5-e6fe8d3614f2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9214d259-df02-492c-b16e-d44b2bc2dd3a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2940c2d9-7755-4dd4-80f5-e6fe8d3614f2",
                    "LayerId": "3b6e9104-023c-4e51-af64-bdcec9a05811"
                }
            ]
        },
        {
            "id": "53940540-9c5f-4a43-a59e-b431ca2e6778",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cb7dddda-4d94-47fa-9c32-fecff94cb467",
            "compositeImage": {
                "id": "553662b9-275e-4aa6-be53-e97c9e5020a4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "53940540-9c5f-4a43-a59e-b431ca2e6778",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "25a0bac9-ec5f-4f54-a359-05d710c89575",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "53940540-9c5f-4a43-a59e-b431ca2e6778",
                    "LayerId": "3b6e9104-023c-4e51-af64-bdcec9a05811"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 485,
    "layers": [
        {
            "id": "3b6e9104-023c-4e51-af64-bdcec9a05811",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cb7dddda-4d94-47fa-9c32-fecff94cb467",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 25,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 392,
    "xorig": 0,
    "yorig": 0
}